--
-- File generated with SQLiteStudio v3.1.1 on mar. jul. 17 16:53:05 2018
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: area
CREATE TABLE area (idarea INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, descripcionarea VARCHAR (100) NOT NULL);

-- Table: multimedia
CREATE TABLE multimedia (idreporte INTEGER NOT NULL REFERENCES reporte (idreporte), tipomultimedia INTEGER NOT NULL REFERENCES tipoArchivo (idtipo), archivo VARCHAR (400) NOT NULL);

-- Table: reporte
CREATE TABLE reporte (idreporte INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, idusuario INTEGER REFERENCES usuario (id) NOT NULL, fecha VARCHAR (50) NOT NULL, hora VARCHAR (10) NOT NULL, estado VARCHAR (50) NOT NULL, ubicacion INTEGER NOT NULL REFERENCES ubicacion (idubicacion), area INTEGER NOT NULL REFERENCES area (idarea), descripcion VARCHAR (250) NOT NULL, cantidadapoyo INT (10) NOT NULL, cantidadcomentarios INT (10) NOT NULL);

-- Table: secuencia
CREATE TABLE secuencia (idsecuencia INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, idreporte INTEGER REFERENCES reporte (idreporte) NOT NULL, fecha VARCHAR (50) NOT NULL, hora VARCHAR (10) NOT NULL, descripcion VARCHAR (250) NOT NULL, comentario VARCHAR (250) NOT NULL, fechacomentario VARCHAR (50) NOT NULL, horacomentario VARCHAR (10) NOT NULL);

-- Table: tipoArchivo
CREATE TABLE tipoArchivo (idtipo INTEGER NOT NULL PRIMARY KEY, descripcion VARCHAR (100) NOT NULL, tammax VARCHAR (100) NOT NULL);

-- Table: ubicacion
CREATE TABLE ubicacion (idubicacion INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, colonia VARCHAR (50) NOT NULL, codigopostal VARCHAR (5) NOT NULL, nombrelocalidad VARCHAR (100) NOT NULL, latitud DOUBLE NOT NULL, longitud DOUBLE NOT NULL, nombremunicipio VARCHAR (100) NOT NULL, claveestado VARCHAR (100) NOT NULL, nombreestado VARCHAR (100) NOT NULL);

-- Table: usuario
CREATE TABLE usuario (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, correo VARCHAR (50) NOT NULL, contra VARCHAR (50) NOT NULL, nombre VARCHAR (25) NOT NULL, apepaterno VARCHAR (25) NOT NULL, apematerno VARCHAR (25) NOT NULL, edad INT (10) NOT NULL, genero VARCHAR (10) NOT NULL);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
